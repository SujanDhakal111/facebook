import React from "react";
import "./Login.css"
import {auth, provider} from "./firebase";
import {useStateValue} from "./StateProvider";
import {actionTypes} from "./Reducer";

function Login () {
    const [state, dispatch] = useStateValue();

    const signIn = () => {
        auth
            .signInWithPopup(provider)
            .then((result) =>{
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
                console.log(result);
            })
            .catch((error) => alert(error.message));

    };
    return(
        <div className="login">
            <div className="login_logo">
                <img
                    src="https://facebookbrand.com/wp-content/uploads/2019/04/f_logo_RGB-Hex-Blue_512.png?w=512&h=512"
                    alt=""/>
            </div>
            <button type="submit" onClick={signIn}>
                Sign In
            </button>

        </div>
    )
 }
 export default Login
