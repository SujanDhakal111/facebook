import React from "react";
import "./StoryReel.css"
import Story from "./Story";

function StoryReel() {
    return (
        <div className="storyReel">
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Sujan"/>
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Subeg"/>
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Suraj"/>
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Shreejit"/>
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Surya"/>
            <Story
                image="https://picsum.photos/200"
                profileSrc="https://picsum.photos/300"
                title="Ujjwal"/>

        </div>
    )
}

export default StoryReel
