
import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyAjuIyNdipQ7BLcKMV41nRKyq_prKbveck",
    authDomain: "facebook-245fc.firebaseapp.com",
    databaseURL: "https://facebook-245fc.firebaseio.com",
    projectId: "facebook-245fc",
    storageBucket: "facebook-245fc.appspot.com",
    messagingSenderId: "703539173251",
    appId: "1:703539173251:web:55d34d72532a8685c588b6",
    measurementId: "G-BGS7V02HSX"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {auth, provider};
export default db;
